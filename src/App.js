import React from 'react';
import './App.css';
import {BrowserRouter as Router, Switch, NavLink, Route} from 'react-router-dom'
import {HomePage, About, Notes, Note, NewNote, ChangeNote, NewList, Lists, SingleList} from './components'
import {Header} from './commons'
import {Provider} from 'react-redux'
import store from './redux'
import './components/bootstrap.min.css'

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Header/>
          <Switch>
            <Route path="/" exact component={HomePage}/>
            <Route path="/about" component={About}/>
            <Route path="/notes" component={Notes}/>
            <Route path="/lists" component={Lists}/>
            <Route path="/note/:index" component={Note}/>
            <Route path="/list/:index" component={SingleList}/>
            <Route path="/NewNote" component={NewNote}/>
            <Route path="/NewList" component={NewList}/>
            <Route path="/changenote/:index" component={ChangeNote}/>
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
