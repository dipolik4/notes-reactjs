import React from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'


const mapStateProps = (store) => {
    let listsArr = [...store.lists]
    return {
        listsArr
    }
}

export const SingleList = connect(mapStateProps) ((props)=>{
    console.log(props)
    console.log(props.listsArr[props.match.params.index].title)
    let listIndex = props.match.params.index;
    console.log(listIndex)

    return(
        <div className="container mx-auto mt-5">
            <h2>{props.listsArr[props.match.params.index].title}</h2>
            <p>{props.listsArr[props.match.params.index].text}</p>
        <div className="row mt-5">
            <div className="col-4 mx-auto">
                <Link to="/lists" type="button" className="btn btn-primary">Go Back</Link>
            </div>
            <div className="col-4 mx-auto">
                {/* <Link to={"/changenote/"+listsIndex} type="button" className="btn btn-primary" >Change Note</Link>              */}
            </div>
        </div>

        </div>
    )
})