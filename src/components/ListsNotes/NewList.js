import React, {useState, useEffect} from 'react'
import {connect} from 'react-redux'
import {TaskInput} from './TaskInput'
import {randomId} from 'random-id'


const mapStateProps = (state) => {
    return({
        lists: state.lists
    })
}

export const NewList = connect(mapStateProps, {}) ((props) => {
//=================================random id module
    var randomId = require('random-id');
    var len = 30;
    var pattern = 'aA0'
    var myRandomId = randomId(len, pattern)    
//================================================
    const [formData, setFormData] = useState ({
        id: 3,
        title: '',
        listsTask: [
            {
                checked: false,
                taskText: '1',
                taskId: ''
            }
        ]
    })
    

    const deleteItem = e => {
        e.preventDefault();
        let arr = formData.listsTask
        let taskToDelete = e.target.parentNode.parentNode.id
        let arrAfterDelete = arr.filter(el => el.taskId !== taskToDelete)
        console.log(taskToDelete)
        console.log(arrAfterDelete)
        setFormData({
            ...formData,
            listsTask: arrAfterDelete
        })
    }
    
    const addItem = async() => {
        let arr = formData.listsTask.push({
                checked: false,
                taskText: '',
                taskId: myRandomId
            }
        )
        setFormData({
            ...formData,
            arr
        })
        // console.log(formData)
    }

    useEffect(()=>{
        console.log(formData.listsTask)
    })

function createNewList(event) {
    if (document.getElementById('listTitle').value){
        return props.dispatch({
            type: "createNewList",
            payload:{
                id: 3,
                title: document.getElementById('listTitle').value,
                listsTask:[
                    {
                    checked: document.querySelector('.checkbox').checked,
                    taskText: document.querySelector('.task').value
                }
            ]
        }
        })
    }
    console.log(props)
}

const handleInput = event => {
    let text = event.target.value
    console.log("input text changed")
}
    return (
        <div className="container">
            <div className="row mt-5">
                <div className="col">
                    <h1 className="text-center">Create new list</h1>
                </div>
            </div>
        <div className="inputs-wrapper">
            <div className="input-group mb-2">
                <div className="input-group-prepend">
                    <div className="input-group-text">Title</div>
                </div>
                <input type="text" className="form-control" id="listTitle" placeholder="Title"/>
            </div>
            <div className="col" id="tasks-wrapper">
                {formData.listsTask.map((item, index) => {
                    return (
                        <TaskInput id={item.taskId} itemKey={index} deleteItem={deleteItem} textChanging={handleInput}/>
                    )
                })
                }    
            </div>
        </div>
        <div className="row text-center">
            <div className="col text-center">
                <button type="button" id="addBtn"  className="btn btn-outline-success mt-4" onClick={addItem}>add</button>                        
                <button type="button" id="saveBtn" className="btn btn-danger mt-4" onClick={createNewList}>Save</button>
            </div>
        </div>
        </div>
        )
    }
)