import React from 'react'
import {connect} from 'react-redux'
import {SingleList} from './SingleList'
import {Link} from 'react-router-dom'

const mapStateProps = (store) => {
    return {
        ...store
    }
}

export const Lists = connect(mapStateProps)((props)=>{ 
    console.log(props)
    console.log(props.lists[1])    
    const lists = props.lists.map((item, index) => {

        const tasksArray = item.listsTask.map((task) => {
            // console.log(task.text)
            return (<p>{task.taskText}</p>)
        })
        
        return (
            <div index={index}>
                <h2>{item.title}</h2>
                <div>{tasksArray}</div>
                <Link to={"/list/"+index}>Go to list item</Link>
            </div>
        )
    })

    return (
        <div>
            {lists}
        </div>        
        )
})