import React from 'react'

export const TaskInput = (props) => {
    let {itemKey, deleteItem, id, click, textChanging, value} = props
    return(
      <div  className="input-group mb-3 task-item" id={id} index={itemKey}>
        <div className="input-group-prepend">
          <div className="input-group-text">
            <input type="checkbox" className="checkbox" onChange={textChanging}/>
          </div>
        </div>
        <input type="text" className="form-control task" aria-label="Text input with checkbox"/>
        <div className="input-group-prepend">
            <div className="btn btn-danger remove-task" onClick={deleteItem} >-</div>
        </div>
      </div>
    )
}