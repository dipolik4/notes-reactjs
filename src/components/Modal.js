import React from 'react'
import {Link} from 'react-router-dom'


export const Modal = (props) => {
    //получили фун-ю изменения через пропс
    return(
        <div className="modal" style={{display:"block"}} tabIndex="1" role="dialog">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Modal title</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <p>Вы уверенны что хотите сохранить изменения? </p>
            </div>
            <div className="modal-footer">
              <Link to="/notes" type="button" className="btn btn-primary"  onClick={props.saveHandler}>Save changes</Link>
              <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={props.modalCloseHandler} >Close</button>
            </div>
          </div>
        </div>
      </div>
    )
}