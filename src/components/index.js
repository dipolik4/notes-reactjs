export {HomePage} from './HomePage';
export {About} from './About';
export {Notes} from './Notes/Notes';
export {Lists} from './ListsNotes/Lists';
export {Note} from './Notes/Note';
export {SingleList} from './ListsNotes/SingleList';
export {NewNote} from './Notes/NewNote';
export {ChangeNote} from './Notes/ChangeNote';
export {NewList} from './ListsNotes/NewList';

