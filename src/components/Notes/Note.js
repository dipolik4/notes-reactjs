import React from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'


const mapStateProps = (store) => {
    let notesArr = [...store.notes]
    return {
        notesArr
    }
}

export const Note = connect(mapStateProps) ((props)=>{
    console.log(props)
    console.log(props.notesArr[props.match.params.index].title)
    let noteIndex = props.match.params.index;
    console.log(noteIndex)

    return(
        <div className="container mx-auto mt-5">
            <h2>{props.notesArr[props.match.params.index].title}</h2>
            <p>{props.notesArr[props.match.params.index].text}</p>
        <div className="row mt-5">
            <div className="col-4 mx-auto">
                <Link to="/notes" type="button" className="btn btn-primary">Go Back</Link>
            </div>
            <div className="col-4 mx-auto">
                <Link to={"/changenote/"+noteIndex} type="button" className="btn btn-primary" >Change Note</Link>             
            </div>
        </div>

        </div>
    )
})