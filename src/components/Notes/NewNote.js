import React from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'


const mapStateProps = (store) => {
}

export const NewNote = connect() ((props) => {
    console.log(props)
    function createNote(event){
        if (document.querySelector('.note-title').value){
        return props.dispatch({
            type: "createNewNote",
            payload: {
                id: Date.now(),
                title: document.querySelector('.note-title').value,
                text: document.querySelector('.note-text').value
            }
        })
    }
    }

    return (
    <div>
        <h1>New Note</h1>
        <form>
            <div className="form mx-auto col-6">
                <div className="col mt-2">
                    <input type="text" className="form-control note-title" placeholder="Note Title"/>
                </div>
                <div className="col mt-2">
                    <textarea type="text" className="form-control note-text" placeholder="Your Text"/>
                </div>
            </div>
            <div className="row mt-5">
                <div className="col-4 mx-auto">
                    <Link to="/notes" type="button" className="btn btn-primary">Go Back</Link>
                </div>
                <div className="col-4 mx-auto">
                    <Link to="/notes" type="button" className="btn btn-primary" onClick={createNote}>Create Note</Link>                
                </div>

            </div>
        </form>

    </div>
    

    )
}
)