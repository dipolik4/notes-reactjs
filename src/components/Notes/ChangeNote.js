import React, {useEffect, useRef, useState} from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import {Modal} from '../Modal'



const mapStateProps = (store) => {
    let notesArr = [...store.notes]
    return {
        notesArr
    }
}

export const ChangeNote = connect(mapStateProps) ((props)=>{
//for modal
    let [ modal,  setModal] = useState(false)

function closeModal() {
    setModal(false)
}

// for dispatch
 let title = useRef()
 let text = useRef()
 let index = props.match.params.index
    
 useEffect(()=>{
        title.current.value = props.notesArr[index].title  
        text.current.value= props.notesArr[index].text
        console.log(title)
    }, [])

    function changeNote () {
        return props.dispatch({
            type: "changeNote",
            payload: {
                index: index,
                title: document.querySelector('.note-title').value,
                text: document.querySelector('.note-text').value
            }
        })
    }

    return(
        <div className="container mx-auto mt-5">
                <div>
        <h1>Change Note</h1>
        <form>
            <div className="form mx-auto col-6">
                <div className="col mt-2">
                    <input type="text" className="form-control note-title" ref={title}/>
                </div>
                <div className="col mt-2">
                    <textarea type="text" className="form-control note-text" ref={text}/>
                </div>
            </div>
        </form>
    </div>
        
        <div className="row mt-5">
            <div className="col-4 mx-auto">
                <Link to="/notes" type="button" className="btn btn-primary">Go Back</Link>
            </div>
            <div className="col-4 mx-auto">
                <button type="button" className="btn btn-primary" onClick={()=>{setModal(true)}} >Change Note</button>             
            </div>
        </div>
        { (modal) ? <Modal saveHandler={changeNote} modalCloseHandler={closeModal}/> : null }
        </div>
    )
    
})
