import React from 'react'
import {Link} from 'react-router-dom'

export const SingleNote = ({title, text, index}) => {
    let link = `/note/${index}`
    
    return(
        <div  style={{
            padding: "15px",
            border: "1px solid gray"
        }}>
            <h2>{title}</h2>
            <p>{text}</p>
            <Link to={link}>Open this NOTE</Link>
        </div>
    )
}