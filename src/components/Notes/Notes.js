import React from 'react'
import {connect} from 'react-redux'
import {SingleNote} from './SingleNote'


const mapStateProps = (store) => {
    return {
        ...store
    }
}

export const Notes = connect(mapStateProps) ((props) => {
    const notesList = props.notes.map((item, index)=>{
        return (
            <SingleNote title={item.title} text={item.text} key={item.id} index={index} />            
        )
    })
    return (
        <div>
            {notesList}
        </div>        
        )
})
