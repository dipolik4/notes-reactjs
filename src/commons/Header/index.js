import React from 'react'
import {NavLink} from 'react-router-dom'
import './style.sass'

export const Header = (props) => {
    return(
    <header>
        <nav>
          <NavLink exact to="/">Home Page</NavLink>
          <NavLink to="/about">About</NavLink>
          <NavLink to="/notes">Notes</NavLink>
          <NavLink exact to="/newnote">Create Note</NavLink>
          <NavLink exact to="/newlist">Create List</NavLink>
        </nav>
    </header>
    )
}