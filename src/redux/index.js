import {createStore} from "redux"

let startState;
if(localStorage.getItem("state")){
    startState = JSON.parse(localStorage.getItem("state"))
console.log(startState )
} else {
    startState = {
        notes: [
            {
                id:1,
                title: 'Tikfb hdfvniew dkju',
                text: 'Lorem ipsun ddn iduy idufnmo. DidunJkjnd Ikjdnd doind'
            },
            {
                id:2,
                title: 'Title 2',
                text: 'Lorem ipsun ddn iduy idufnmo. DidunJkjnd Ikjdnd doind'
            },
            {
                id:3,
                title: 'Title 3  dkju',
                text: 'Lorem ipsun ddn iduy idufnmo. DidunJkjnd Ikjdnd doind'
            }
        ],
        lists: [
            {
                id:1,
                title: "1st List Title",
                listsTask: [
                    {
                    checked:false,
                    taskText: "Some item text"
                    },
                    {
                    checked:true,
                    taskText: "Some item text 2"
                    }
            ]
        },
        {
            id:2,
            title: "2nd List Title",
            listsTask: [
                {
                checked:false,
                taskText: "Some slkdjhkjhs item text"
                },
                {
                checked:true,
                taskText: "Someskjhdkjhskj item text 2"
                }
        ]
    }
    
    ]
    }
}

function reducer(state = startState, action) {
    const {type, payload} = action;
    switch(type) {
        case "createNewNote":
            // return {
                // notes: [...state.notes, payload]
            // }
            // console.log(payload)


            //без деструктуризации            
            let newState = Object.assign({}, state)
            newState.notes.push(payload)
            localStorage.setItem("state", JSON.stringify(newState))
            return newState;
            break;

        case "createNewList":
            let newlistState = Object.assign({}, state)
            newlistState.lists.push(payload)
            localStorage.setItem("state", JSON.stringify(newlistState))
            console.log(newlistState)
            return newlistState;
            break;
            
        case 'changeNote':
            console.log(payload.index)
            console.log(state.notes)
            let changedState = Object.assign({}, state)
            changedState.notes[payload.index].title = payload.title
            changedState.notes[payload.index].text = payload.text
            console.log("OK")
            return changedState 
            
            break;

        default:
            return state
            
    }
}


const store = createStore(
    reducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  );

export default store